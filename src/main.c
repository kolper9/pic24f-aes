/*
 * File:   newmainXC16.c
 * Author: kolpe
 *
 * Created on February 21, 2019, 10:20 PM
 */


 #include "xc.h"
// Konfigurace pro FCY = 16MHz
// a dal?� nutn� implicitn� nastaven�
// Konfigurace pro FCY = 16MHz
// a dal?� nutn� implicitn� nastaven�

#pragma config JTAGEN=OFF, GCP=OFF, GWRP=OFF, FWDTEN=OFF, ICS=PGx2, \
               IESO=OFF, FCKSM=CSDCMD, OSCIOFNC=OFF, POSCMOD=HS, \
               FNOSC=PRIPLL, PLLDIV=DIV3, IOL1WAY=ON

#include "usbserial.h"
#include "delay.h"
#include "display.h"
#include <stdio.h>

unsigned char KEY[4][4] = { {0x2b, 0x7e, 0x15, 0x16}, 
                            {0x28, 0xae, 0xd2, 0xa6}, 
                            {0xab, 0xf7, 0x15, 0x88}, 
                            {0x09, 0xcf, 0x4f, 0x3c} };

const unsigned  char SBOX[256] =
{
  0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
  0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
  0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
  0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
  0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
  0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
  0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
  0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
  0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
  0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
  0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
  0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
  0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
  0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
  0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
  0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
};
unsigned char MIX_C_TABBLE [4][4] = {{2, 3, 1, 1},
                                     {1, 2, 3, 1},
                                     {1, 1, 2, 3},
                                     {3, 1, 1, 2}};

unsigned char RCONS [10] = { 0x01,0x02,0x04,0x08,0x10, 0x20, 0x40 , 0x80,0x1b,0x36};



void init_UART (void){
    
    U1MODEbits.UARTEN=1; 
    U1MODEbits.USIDL=0;
    U1MODEbits.WAKE=1;
    U1MODEbits.UEN0=1;
    U1MODEbits.UEN1=0;
    U1MODEbits.IREN=0;
    U1MODEbits.ABAUD=0;
    U1MODEbits.LPBACK=0;
    U1MODEbits.RTSMD=0;
    U1MODEbits.STSEL=0;
    U1MODEbits.PDSEL=0;
    U1MODEbits.BRGH=1;
      
    U1BRG=417;
    
    U1STAbits.UTXISEL0 = 0;            // Interrupt after one TX character is transmitted
    U1STAbits.UTXISEL1 = 0;
   
    U1MODEbits.UARTEN = 1;              // Enable UART
    U1STAbits.UTXEN = 1;
    RPOR12bits.RP24R=3;
    RPINR18bits.U1RXR = 22;
    delay_loop(105);  
}
void init_components (void){
    usbserial_init();
    disp_init(); 
    set_color(1);  
    init_UART();
}
//Subbyte table with sbox 
void subbytes( unsigned char  crypt_table[][4] ){
   int i,j;
   for(i=0;i<4;i++){
    for(j=0;j<4;j++){
      crypt_table[i][j]=SBOX[(int)crypt_table[i][j]];
    }
   }            
}

//Subbyte vector with sbox 
void subbytesv( unsigned char  crypt_table[] ){
  int i;
   for(i=0;i<4;i++){
      crypt_table[i]=SBOX[(int)crypt_table[i]];
   }          
}


void shift_rows (unsigned char  crypt_table[][4]){
   unsigned char first,second,last;
   int i=0;
  
   //Second row
   first=crypt_table[0][1];
   for(i=0;i<3;i++){  
     crypt_table[i][1]=crypt_table[i+1][1];   
   }
   crypt_table[i][1]=first;
    
    //Third row
   first=crypt_table[0][2];
   second=crypt_table[1][2];
   for(i=0;i<2;i++){
    crypt_table[i][2]=crypt_table[i+2][2];  
   }
   crypt_table[2][2]=first;
   crypt_table[3][2]= second;
   
   //Fourth row
   last=crypt_table[3][3];
   for(i=3;i>0;i--){
       crypt_table[i][3]=crypt_table[i-1][3];   
   }
   crypt_table[0][3]= last;
   
}

void RotWord( unsigned char  crypt_vector[],int crypt_vector_lenght){
   unsigned char first;
   int i=0;
   
   first=crypt_vector[0];
   for(i=0;i<crypt_vector_lenght-1;i++){  
     crypt_vector[i]=crypt_vector[i+1];   
   }
   crypt_vector[i]=first;
}

void add_key ( unsigned char  crypt_table[][4] , unsigned char key[][4]){
   int i,j;
   for(i=0;i<4;i++){
    for(j=0;j<4;j++){
      crypt_table[i][j]^=key[i][j];
    }
   }
}

void mix_columns ( unsigned char  crypt_table[][4]){
    int i=0,j=0,k=0;
    unsigned char  new_column[4];
    unsigned char res;
    for(k=0;k<4;k++){
        for(i=0;i<4;i++){
            
            new_column[i]=0;
            for(j=0;j<4;j++){
                 
                res=0;
                //Multiply with matrix and add 1b of most significant bit is one
                
                 if(MIX_C_TABBLE[i][j]==0x01){
                     res^=crypt_table[k][j];
                 }
                //Multiplying with two is shifleft 
                 if(MIX_C_TABBLE[i][j]==0x02){
                     res^=(crypt_table[k][j]<<1);
                     if(crypt_table[k][j]&0x80){
                        res^=0x1b; 
                     }
                 }
                 //Multiplying with two is shifleft and xor with original value
                 if(MIX_C_TABBLE[i][j]==0x03){
                      res^=(crypt_table[k][j]<<1);
                      res^=crypt_table[k][j];
                      if(crypt_table[k][j]&0x80){
                        res^=0x1b; 
                     }
                 }
              //Xor to temporary column e 
              new_column[i]^=res; 
            }
          
        } 
        for(j=0;j<4;j++){
            //Copy temporary column to table
            crypt_table[k][j]=new_column[j];
        }
      
    }
}

unsigned char serial_read (){
    while( U1STAbits.URXDA == 0 ){
          }
  return (unsigned char) U1RXREG;
}

void serial_write_int_hex ( int print, int precision){
    char to_print[precision];
    int i=0; 
    
    snprintf(to_print, precision, "%X",print);
    while(to_print[i]!='\0' ){ 
       U1TXREG = to_print[i];
       i++;
       while(U1STAbits.UTXBF == 1){
       }
    }
    
}
void print_int_hex ( int print, int precision ){
    char to_print[precision];
    snprintf(to_print, precision, "%X",print);
    disp_str(to_print);
}



void key_expansion( unsigned char  key[][4], const int number_of_round){
  int i,j;
  unsigned char crypt_vector[4];
  
  for(i=0;i<4;i++){
      crypt_vector[i]=key[3][i];
  }

  RotWord(crypt_vector,4);
  subbytesv(crypt_vector);
  crypt_vector[0]^=RCONS[number_of_round];
  
  for(j=0;j<4;j++){
    key[0][j]^=crypt_vector[j];
  }
  
  for(i=1;i<4;i++){
    for(j=0;j<4;j++){
       key[i][j]^=key[i-1][j];
     }
   }   
}

 unsigned char * aes_encrypt( const unsigned char  to_encrypt[]){

unsigned char  key[4][4];
unsigned char crypt_table[4][4];
static unsigned char crypted_table[16];
int number_of_round=0;
int i,j;

//create temporary key
for(i=0;i<4;i++){
  for(j=0;j<4;j++){
   key[i][j]=KEY[i][j];
 }
}

//create temporary encrypt table
for(i=0;i<4;i++){
  for(j=0;j<4;j++){
    crypt_table[i][j]=to_encrypt[i*4+j];
 }
}

add_key(crypt_table,key);

for(i=0;i<9;i++){  
    subbytes(crypt_table);
    shift_rows(crypt_table);
    mix_columns(crypt_table);
    key_expansion(key,number_of_round);
    add_key(crypt_table,key);
    number_of_round++;
}
    //Last round 
    subbytes(crypt_table);
    shift_rows(crypt_table);e
    key_expansion(key,number_of_round);
    add_key(crypt_table,key);
    
    //copy encrypted table to vector to return
    for(i=0;i<4;i++){
      for(j=0;j<4;j++){
       crypted_table[4*i+j]=crypt_table[i][j];
     }
    }
    return crypted_table ;   
}


int main(void) {
    
    delay_loop(10000);
    init_components();
    delay_loop(10000);
  
    serial_read();
    
    int char_count=0;
    
    unsigned char to_enc[16] = {0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
   
    while(1){
    unsigned char function_num=serial_read(); //Read function number
      if(function_num==0x01){  //Crypt with AES 128
        int i;
        unsigned char * enc;
        while(char_count<16){
            to_enc[char_count]=serial_read();
            char_count++;
        }
               
        
        
        enc=aes_encrypt(to_enc);
        
        for ( i=0;i<16;i++){
            print_int_hex((int)enc[i],6);
            serial_write_int_hex((int)enc[i],6);
        }
        disp_char('\n');
        char_count=0;
      }
      if(function_num==0x02){   //Change key 128
          int char_count=0;
          while(char_count<16){
            KEY[char_count/4][char_count%4]=serial_read();
            char_count++;
          }
           int i;
           for ( i=0;i<16;i++){
            print_int_hex(KEY[i/4][i%4],6);
            serial_write_int_hex((int)KEY[i/4][i%4],6);
           }
            disp_char('\n');
          
          
      }
   }

    return 0;
}
